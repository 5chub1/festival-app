import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SearchModalComponent } from '../search-modal/search-modal.component';

@Component({
  selector: 'app-bottom-toolbar',
  templateUrl: './bottom-toolbar.component.html',
  styleUrls: ['./bottom-toolbar.component.scss']
})
export class BottomToolbarComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

    openDialog(): void {
      this.dialog.open(SearchModalComponent, {});
    }
}

