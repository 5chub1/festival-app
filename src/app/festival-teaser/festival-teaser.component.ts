import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-festival-teaser',
  templateUrl: './festival-teaser.component.html',
  styleUrls: ['./festival-teaser.component.scss'],

  styles: ['.active {color: #ffb700;}']


})
export class FestivalTeaserComponent implements OnInit {
  public isActive = false;

  constructor() { }

  like() {
    console.log('Test');
    this.isActive = !this.isActive;
  }



  ngOnInit() {
  }

}
