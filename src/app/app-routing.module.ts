import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FestivalMicrositeComponent } from './festival-microsite/festival-microsite.component';
import { FestivalTeaserComponent } from './festival-teaser/festival-teaser.component';


const routes: Routes = [
  { path: '', component: FestivalTeaserComponent},
  { path: 'festival-microsite', component: FestivalMicrositeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ FestivalMicrositeComponent, FestivalTeaserComponent ];
