import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatCardModule, MatButtonModule, MatToolbarModule, MatIconModule, MatChipsModule } from '@angular/material';
import { MatFormFieldModule, MatSliderModule, MatDialogModule } from '@angular/material';
import { FestivalListComponent } from './festival-list/festival-list.component';
import { HeaderComponent } from './header/header.component';
import { BottomToolbarComponent } from './bottom-toolbar/bottom-toolbar.component';
import { SearchModalComponent } from './search-modal/search-modal.component';




@NgModule({
   declarations: [
      AppComponent,
      FestivalListComponent,
      HeaderComponent,
      BottomToolbarComponent,
      SearchModalComponent,
      routingComponents
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      MatInputModule,
      MatCardModule,
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatChipsModule,
      MatFormFieldModule,
      MatSliderModule,
      MatDialogModule
   ],
   providers: [],
   bootstrap: [
      AppComponent,
      HeaderComponent,
      BottomToolbarComponent,
      SearchModalComponent
   ]
})
export class AppModule { }
